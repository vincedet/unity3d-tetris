﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameOver : MonoBehaviour {

	float timer = 0.0f;

	void Start () {
	
	}

	void Update () {

		if (Input.GetKey("escape"))
			Application.Quit();
		
		timer += Time.deltaTime;
		if (timer > 5)
			SceneManager.LoadScene ("MainGame");
	}
}
