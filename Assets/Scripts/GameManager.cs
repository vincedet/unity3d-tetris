﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {
	
	private float time;
	private Vector3 fall;
	private Vector3 moveLeft;
	private Vector3 moveRight;

	static GameObject[,] GameFieldMatrix;

	#region Public Fields
	public int columns = 10;
	public int rows = 21; 
	public ParticleSystem explosion;
	public TextMesh gameOverText;
	public Block[] allBlockPieces;
	public KeyCode rotateButton;
	public KeyCode leftButton;
	public KeyCode rightButton;
	public KeyCode speedUpButton;
	#endregion

	#region Private Fields
	private bool gameOver;
	private Block currentBlock;
	private int rotationIndex;
	private float rotationAngle = 90;
	#endregion


	void Awake () {
		// initialize game matrix
		GameFieldMatrix = new GameObject[rows, columns];

		// initialize movement vectors
		fall = new Vector3 (0, -0.64f, 0);
		moveLeft = new Vector3 (-0.64f, 0, 0);
		moveRight = new Vector3 (0.64f, 0, 0);
	}

	void Start () {
		gameOver = false;
		time = .4f;
		Time.fixedDeltaTime = time;

		// place 1st block of the game
		currentBlock = spawnNext ();
	}

	// Update is called once per frame
	void Update() {

		if (gameOver) {
			// Load "GameOver" Scene
			GameOver ();
		}

		if (Input.GetKeyUp (rotateButton)) {
			if(CanRotate)
				currentBlock.Rotate (rotationAngle);
		}

		if (Input.GetKeyUp (leftButton)) {
			if (CanMoveLeft)
				currentBlock.transform.position += moveLeft;
		}

		if (Input.GetKeyUp (rightButton)) {
			if (CanMoveRight)
				currentBlock.transform.position += moveRight;
		}

		if (Input.GetKeyDown (speedUpButton)) {
			if(CanMoveDown)
				currentBlock.transform.position += fall;
		}
	}

	// Function GameOver calls the "GameOver" scene
	void GameOver()
	{	
		SceneManager.LoadScene ("GameOver");
	}

	void FixedUpdate () {
		
		if (CanMoveDown) {
			currentBlock.transform.position += fall;
		}
		else {

			// update game matrices
			UpdateGameMatrices();

			// check if some rows has to be cleared
			ArrayList lines = LoadExistingLines();

			// if lines exist
			if (lines.Count > 0) {
				// lines destruction animation
				foreach (int line in lines) {
					for (int i = 0; i < columns; i++) {
						var boom = Instantiate (explosion);
						boom.transform.position = GameFieldMatrix [line, i].transform.position;
						//explosion.transform.position.Set(GameFieldMatrix [line, i].transform.position.x, GameFieldMatrix [line, i].transform.position.y, z);
						boom.transform.rotation = Quaternion.identity;
						boom.Play ();
						Destroy (boom.gameObject, boom.duration);

						// BOOOOOM
						Destroy(GameFieldMatrix [line, i]);
						GameFieldMatrix [line, i] = null;
					}
				}
				// let lines above fall
				foreach (int line in lines) {
					if(line > 0)
						LetLinesAboveFall (line - 1);
				}
			}

			// load new Block
			currentBlock = spawnNext ();
		
		}
	}

	void LetLinesAboveFall(int startLine) {
		for (int y = startLine; y > 0; y--) {
			for (int x = 0; x < columns; x++) {
				GameFieldMatrix [y + 1, x] = GameFieldMatrix [y, x];
				// translate down if there is a block to translate
				if (GameFieldMatrix [y + 1, x] != null) {
					GameFieldMatrix [y + 1, x].transform.position += fall;
				}
			}
		}
	}

	// Return list of y indexes that correspond to lines to be cleared in the GameSpaceMatrix
	ArrayList LoadExistingLines() {

		ArrayList lines = new ArrayList ();
		 
		for (int y = 0; y < rows; y++) {
			int count = 0;
			for (int x = 0; x < columns; x++) {
				if (GameFieldMatrix [y, x] != null)
					count++;
			}

			if (count == columns)
				lines.Add (y);
		}

		return lines;
	}

	// Save actual game objects in the game field matrix
	void UpdateGameMatrices (){
		
		for (int i = 0; i < currentBlock.transform.childCount; i++) 
		{
			if(currentBlock.transform.GetChild (i).name == "Pivot")
				continue;
			
			float yLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.y;
			float xLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.x;

			int yIndex = CalculateYIndex(yLocation);
			int xIndex = CalculateXIndex(xLocation);

			if(yIndex <= 0)
			{
				gameOver = true;
				break;
			}

			GameFieldMatrix [yIndex - 1, xIndex] = currentBlock.transform.GetChild(i).gameObject;
			// remove rotation pivot
			currentBlock.RemovePivot ();
		}
	}

	// generate a new random block and place it to its start position
	public Block spawnNext() {
		
		// Random Index
		int i = Random.Range(0, allBlockPieces.Length);

		// Spawn Block at Position i
		Block newBlock = (Block)Instantiate(allBlockPieces[i],
			new Vector2 (Mathf.Round(columns / 2) *0.64f, rows*0.64f),
			Quaternion.identity);

		return newBlock;
	}

	// returns true if the Block object can fall one level below
	private bool CanMoveDown {
		get {
			bool canMove = true;
			for (int i = 0; i < currentBlock.transform.childCount; i++) 
			{
				if(currentBlock.transform.GetChild (i).name == "Pivot")
					continue;
				float yLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.y;
				float xLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.x;

				int yIndex = CalculateYIndex(yLocation);
				int xIndex = CalculateXIndex(xLocation);

				if (yLocation < 0) {
					canMove = false;
					break;
				} else {
					if (GameFieldMatrix [yIndex, xIndex] != null) {
						canMove = false;
						break;
					}
				}
			}
			return canMove;
		}
	}

	// returns true if the Block object can be translated one position to the right
	private bool CanMoveRight {
		get {
			bool canMove = true;
			for (int i = 0; i < currentBlock.transform.childCount && canMove; i++) {
				Transform child = currentBlock.transform.GetChild (i);
				if (child.name == "Pivot")
					continue;

				 
				float yLocation = child.gameObject.transform.position.y;
				float xLocation = child.gameObject.transform.position.x;

				int yIndex = CalculateYIndex (yLocation);
				int xIndex = CalculateXIndex (xLocation);

				if (IsOutOfBound(xIndex + 1,yIndex)) {
					canMove = false;
					break;
				}

				if (GameFieldMatrix [yIndex, xIndex + 1] != null){
					canMove = false;
					break;
				}
			}
			return canMove;
		}
	}

	// returns true if the Block object can be translated one position to the left
	private bool CanMoveLeft {
		get {
			bool canMove = true;
			for (int i = 0; i < currentBlock.transform.childCount && canMove; i++) 
			{
				if(currentBlock.transform.GetChild (i).name == "Pivot")
					continue;
				
				float yLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.y;
				float xLocation = currentBlock.transform.GetChild(i).gameObject.transform.position.x;

				int yIndex = CalculateYIndex(yLocation);
				int xIndex = CalculateXIndex(xLocation);

				if(IsOutOfBound(xIndex - 1, yIndex)){
					canMove = false;
					break;
				}

				if(GameFieldMatrix [yIndex, xIndex - 1] != null)
				{
					canMove = false;
					break;
				}
			}
			return canMove;
		}
	}

	// returns true if the Block object can be rotated (angle defined by "rotationAngle")
	private bool CanRotate {
		get {
			bool canMove = true;

			// load current object's pivot
			Transform pivot = currentBlock.transform.Find ("Pivot");

			// Loop over current object's childs to check if one of them CAN'T be rotated
			for (int i = 0; i < currentBlock.transform.childCount && canMove; i++) {
				if (currentBlock.transform.GetChild (i).name == "Pivot")
					continue;
				
				GameObject go = new GameObject ();
				Transform copy = go.transform;
				copy.transform.position = currentBlock.transform.GetChild (i).transform.position;
				copy.transform.rotation = currentBlock.transform.GetChild (i).transform.rotation;
				copy.transform.localScale = currentBlock.transform.GetChild (i).transform.localScale;
				if (pivot)
					copy.transform.RotateAround (pivot.position, new Vector3 (0, 0, 1), rotationAngle);
				
				int xIndex = CalculateXIndex (copy.position.x);
				int yIndex = CalculateYIndex (copy.position.y);

				// Destroy gameobject as it was only used to test the rotation 
				Destroy (go);

				if(IsOutOfBound(xIndex,yIndex)){
					canMove = false;
					break;
				}


				if(GameFieldMatrix[yIndex,xIndex] != null){
					canMove = false;
					break;
				}
			}

			if (!canMove) {
				Debug.Log ("CanRotate - cannot rotate");
			}
			return canMove;
		}
	}

	private int CalculateXIndex(float xTransform)
	{
		int returnIndex;
		float xFloat = xTransform / 0.64f;
		returnIndex = (int)System.Math.Round(xFloat, 0);
		return returnIndex;
	}

	private int CalculateYIndex(float yTransform)
	{
		int returnIndex;
		float yFloat = rows - (yTransform / 0.64f);
		returnIndex = (int)System.Math.Round(yFloat, 0);
		return returnIndex;
	}

	private bool IsOutOfBound(int x, int y)
	{
		if (x < 0 || x >= columns || y < 0 || y >= rows)
			return true;
		return false;
	}
}
