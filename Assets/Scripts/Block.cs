﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a Block Object.
/// 
/// Block implements the Interface IBlock which forces all blocks to define the way they rotate
/// Block specific functions could be added here
/// 
/// </summary>
public class Block : MonoBehaviour, IBlock {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Rotate Block object around its corresponding Pivot
	public void Rotate (float angle) {
		Transform pivot = transform.Find ("Pivot");
		if(pivot)
			transform.RotateAround (pivot.position, new Vector3(0,0,1), angle);
	}

	// Remove pivot object (pivot is useless once the block is at a landed position)
	public void RemovePivot() {

		Transform pivot = transform.Find ("Pivot");
		if (pivot)
			Object.Destroy (pivot.gameObject);
	}

	// Returns true if block is empty or contains only its pivot
	public bool CanBeDestroyed {
		get {
			if(transform.childCount == 0 || (transform.childCount == 1 && transform.Find ("Pivot") != null))
				return true;
			return false;
		}
	}
}
