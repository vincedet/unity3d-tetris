﻿/// <summary>
/// Interface block.
/// 
/// Defines function that all block types of the game have to implement
/// </summary>
public interface IBlock
{
	/*bool CanMoveLeft();
	bool CanMoveRight();
	bool CanMoveUp();
	bool CanMoveDown();
	bool CanRotate();*/

	void Rotate (float angle);
}

